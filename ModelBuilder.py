from keras.applications.resnet_v2 import ResNet50V2
from keras.layers import GlobalAveragePooling2D, Dense
from keras.models import Model
from keras.optimizers import Adam
from keras.losses import CategoricalCrossentropy
from keras.callbacks import LambdaCallback, LearningRateScheduler

class ModelBuilder:
    def __init__(self, num_of_classes, input_shape):
        self.num_of_classes = num_of_classes #number of classes to classify
        self.input_shape = input_shape #Resolution of the images
        #Input tensors should be of shape (Number of training examples, image width, image height, number of channels(RGB = 3))
        pass

    def build_model(self):
        #Load the base model without the final classification layer
        resnet_base_model = ResNet50V2(weights='imagenet', include_top=False, input_shape=self.input_shape)
        #Freeze the whole model
        resnet_base_model.trainable = False
        #Create custom classification layers.
        avg_pool_layer = GlobalAveragePooling2D()(resnet_base_model.output)
        prediction_layer = Dense(units = self.num_of_classes, activation='softmax')(avg_pool_layer)
        #Train the custom layer only
        prediction_layer.trainable = True

        self.model = Model(inputs = resnet_base_model.input, outputs = prediction_layer)
        pass

    def summarize_model(self):
        #Display a summary of the model in the console
        self.model.summary()
        pass

    def compile_the_model(self):
        #Set the training rate of the model. Too slow and it will take too long. Too high and it won't converge. It might even diverge.
        #Maybe consider a learning rate scheduler
        optimizer = Adam(learning_rate=0.00001)
        self.model.compile(loss=CategoricalCrossentropy(), optimizer=optimizer, metrics=['accuracy'])

    def train_the_model(self, train_dataset, validation_dataset):
        #If you want to plot history against epoch later, use history
        my_callback = LambdaCallback(on_epoch_end=self.callback_function)
        #learning_rate_callback = LearningRateScheduler(lambda epoch: 1e-8*10**(epoch/20))
        history = self.model.fit(train_dataset, epochs = 100, validation_data=validation_dataset, shuffle = True, callbacks=[my_callback])
        pass

    def callback_function(self, batch, logs):
        #Called at the end of every training epoch.
        pass




