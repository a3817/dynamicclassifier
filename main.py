import ModelBuilder as mb

#The number of  classes will be set dynamically, depending how many types you want to classifiy
#The input shape depends on the resolution of your input images.
my_model_builder = mb.ModelBuilder(10, (224, 224, 3))
my_model_builder.build_model()
my_model_builder.summarize_model()
my_model_builder.compile_the_model()